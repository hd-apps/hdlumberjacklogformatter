Pod::Spec.new do |s|

  s.name         = "HDLumberjackLogFormatter"
  s.version      = "2.0.0"
  s.summary      = "A Custom CocoaLumberjack logformatter"
  s.homepage     = "http://hd-apps.com"
  s.license      = 'Apache License, Version 2.0'
  s.author             = { "Harold" => "harold@hd-apps.com" }
  s.ios.deployment_target = '7.0'
  s.osx.deployment_target = '10.9'
  s.source       = { :git => "https://hd-apps@bitbucket.org/hd-apps/hdlumberjacklogformatter.git", :tag => s.version.to_s }
  s.source_files  = 'HDLumberjackLogFormatter/Classes/*.{h,m}'
  s.requires_arc = true
  s.dependency 'CocoaLumberjack'

end