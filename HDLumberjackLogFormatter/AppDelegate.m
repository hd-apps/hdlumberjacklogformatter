//
//  AppDelegate.m
//  HDLumberjackLogFormatter
//
//  Created by Harold on 25/02/14.
//  Copyright (c) 2014 hd-apps.com. All rights reserved.
//

#import "AppDelegate.h"
#import "HDLumberjackLogFormatter.h"

//The default log levels for this class
static int ddLogLevel = DDLogLevelVerbose;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    //Add the Terminal and TTY(XCode console) loggers to CocoaLumberjack (simulate the default NSLog behaviour)
    HDLumberjackLogFormatter* logFormat = [[HDLumberjackLogFormatter alloc] init];
    
    DDASLLogger *aslLogger = [DDASLLogger sharedInstance];
    [aslLogger setLogFormatter: logFormat];
    DDTTYLogger *ttyLogger = [DDTTYLogger sharedInstance];
    [ttyLogger setLogFormatter:logFormat];
    [ttyLogger setColorsEnabled:YES];
    
    //Give INFO a color
    UIColor *pink = [UIColor colorWithRed:(250/255.0) green:(50/255.0) blue:(150/255.0) alpha:1.0];
    [[DDTTYLogger sharedInstance] setForegroundColor:pink backgroundColor:nil forFlag:DDLogFlagInfo];
    
    [DDLog addLogger:aslLogger];
    [DDLog addLogger:ttyLogger];
    
    DDLogError(@"This is an ERROR");
    //Gives
    //ERROR   2014/07/30 10:45:41.823 [AppDelegate application:didFinishLaunchingWithOptions:29] This is an ERROR
    //OR with DEBUG defined
    //ERROR   11:09:20.363 [AppDelegate application:didFinishLaunchingWithOptions:29] This is an ERROR
    
    DDLogWarn(@"This is a WARNING");
    DDLogInfo(@"This is INFO");
    DDLogDebug(@"This is DEBUG");
    DDLogVerbose(@"This is VERBOSE\n");
   
    [self testFunctionNoArguments];
    [self testFunctionWithArgument:@"Argument not used"];
    
    return YES;
}

- (void)testFunctionNoArguments {
    DDLogVerbose(@"Log string function no argument");
}

- (void)testFunctionWithArgument:(NSString *)argumentNoUsed {
    DDLogVerbose(@"Log string function with argument, note the : between end of function name and ]");
}

//Functions for changing the default log levels during runtime
#pragma mark -
#pragma mark Lumberjack functions for dynamic logging
+ (int)ddLogLevel {
    return ddLogLevel;
}

+ (void)ddSetLogLevel:(int)logLevel {
    ddLogLevel = logLevel;
}

@end
