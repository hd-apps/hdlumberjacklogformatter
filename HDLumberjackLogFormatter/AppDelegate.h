//
//  AppDelegate.h
//  HDLumberjackLogFormatter
//
//  Created by Harold on 25/02/14.
//  Copyright (c) 2014 hd-apps.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
