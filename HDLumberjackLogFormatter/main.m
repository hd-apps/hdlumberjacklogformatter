//
//  main.m
//  HDLumberjackLogFormatter
//
//  Created by Harold on 25/02/14.
//  Copyright (c) 2014 hd-apps.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
